class Node(object):
    def __init__(self, *args, **kw):
        self.__dict__.update(kw)
        
    def __getattr__(self, key):
        # Missing attribute (not set by the initializer)
        return None
        
class ModuleNode(Node):
    """name = str, imports = [ImportNode], global_vars = [GlobalVarNode], procedures = [ProcedureNode], line = int"""
    pass

class ImportNode(Node):
    """name = str, procedures = [ProcedureNode], line = int"""
    pass

class GlobalVarNode(Node):
    """name = str, value = bool|int|float, line = int"""
    pass

class ProcedureNode(Node):
    """name = str, ret_ty = str, args = [ArgumentNode], body = ['StmtNode'], line = int"""
    pass

class ArgumentNode(Node):
    """name = str, ty = str, is_variable = bool, line = int"""
    pass

class VariableDeclarationNode(Node):
    """name = str, ty = str, line = int"""
    pass

class AssignNode(Node):
    """name = str, value = 'ExprNode', line = int"""
    pass

class IfNode(Node):
    """condition = 'ExprNode', then_stmts = ['StmtNode'], elseifs = [ElseIfNode], else_stmts = ['StmtNode'], line = int"""
    pass

class ElseIfNode(Node):
    """condition = 'ExprNode', then_stmts = ['StmtNode'], line = int"""
    pass

class WhileNode(Node):
    """condition = 'ExprNode', body = ['StmtNode'], line = int"""
    pass

class ForNode(Node):
    """var = str, start = 'ExprNode', stop = 'ExprNode', step = 'ExprNode'|None, body = ['StmtNode'], line = int"""
    pass

class ReturnNode(Node):
    """value = 'ExprNode', line = int"""
    pass

class BinOpNode(Node):
    """left = 'ExprNode', operator = str, right = 'ExprNode', line = int"""
    pass

class UnOpNode(Node):
    """operator = str, value = 'ExprNode', line = int"""
    pass

class CallNode(Node):
    """name = str, args = ['ExprNode'], line = int"""
    pass

class LitteralNode(Node):
    """value = bool|int|float, line = int"""
    pass

class VariableNode(Node):
    """name = str, line = int"""
    pass
