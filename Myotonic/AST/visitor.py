from Myotonic.AST import AST

class ASTVisitor(object):
    def __call__(self, node, *args, **kw):
        """Dispatch a visit to node while passing *args and **kw on to the handler."""
        node_name = node.__class__.__name__
        func_name = "do_" + (node_name[:-4] if node_name.endswith("Node") else node_name)
        if not hasattr(self, func_name): return node
        return getattr(self, func_name)(node, *args, **kw)