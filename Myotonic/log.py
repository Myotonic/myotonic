import sys

def _stderr(prefix, lineno, msg):
    if lineno == None:
        lineno = "?"
    sys.stderr.write("%s: %s: %s\n" % (prefix, lineno, msg))
    
def warn(msg, lineno=None):
    _stderr("WARNING", lineno, msg)
    
def error(msg, lineno=None):
    _stderr("ERROR", lineno, msg)
