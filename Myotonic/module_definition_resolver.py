from Myotonic.AST import AST, visitor
from Myotonic import parser
from Myotonic import log

import os, sys

class ModuleDefinitionResolver(visitor.ASTVisitor):
    def __init__(self, search_path=None):
        if not search_path:
            search_path = []
        default_search_path = [os.curdir]
        search_path = search_path[:] + default_search_path
        
        am = self.available_modules = {}
        for path in map(os.path.abspath, search_path):
            try:
                files = filter(lambda s: s.lower().endswith(".myo.def"), map(lambda f: os.path.join(path, f), next(os.walk(path))[2]))
            except StopIteration:
                continue
            for file in files:
                f = os.path.basename(file)
                am[f[:-8]] = am.get(f[:-8], file)
                
    def find(self, name):
        return self.available_modules.get(name)
        
    def do_Module(self, node):
        success = True
        for imp in node.imports:
            success = self(imp) and success
            
        if not success:
            sys.exit(1)
            
    def do_Import(self, node):
        fpath = self.find(node.name)
        if fpath is None:
            log.error("Unable to find definition for module '%s'." % node.name, node.line)
            return False
            
        try:
            definition = open(fpath).read()
        except IOError:
            log.error("Problems while trying to read '%s'." % fpath, node.line)
            return False
            
        node.procedures = node.procedures or []            
        for fn_name, ret_ty, args in parser.parse_definition(definition):
            proc = AST.ProcedureNode(name=fn_name, ret_ty=ret_ty, args=[], body=None, line=node.line)
            for arg in args:
                proc.args.append(AST.ArgumentNode(name=arg.name, ty=arg.ty, is_variable=arg.is_variable, line=node.line))
            node.procedures.append(proc)
        return True