"""
The Myotonic parser.
"""
from Myotonic.parser.parser import ModuleParser, DefinitionParser

def parse_module(*args, **kw):
    return ModuleParser().parser.parse(*args, **kw)
    
def parse_definition(*args, **kw):
    return DefinitionParser().parser.parse(*args, **kw)
