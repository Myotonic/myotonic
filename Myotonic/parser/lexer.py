import lex                  #Taken from ply package
from Myotonic import log
        
def make_lexer():
    from Myotonic.parser.tokens import tokens
    states = (("comment", "exclusive"),)
    t_ANY_ignore = " \t"
    
    def t_ANY_error(t):
        log.warn("Unknown character %s. Skipping." % repr(t.value[0]), t.lexer.lineno)
        t.lexer.skip(1)
        
    
    #Comments
    def t_line_comment(t):
        r"\/\/.*\n"
        t.lexer.lineno += 1
        pass
    
    def t_ANY_start_comment(t):
        r"\/\*"
        t.lexer.push_state("comment")
        pass
    
    def t_comment_end(t):
        r"\*\/"
        t.lexer.pop_state()
        pass
    
    def t_comment_any(t):
        r"."
        pass
    
    
    def t_ANY_newlines(t):
        r"\n+"
        t.lexer.lineno += len(t.value)
        pass
    
    #Invariant tokens
    def t_MODULE(t):
        r"Module"
        return t
        
    def t_IMPORTS(t):
        r"Imports"
        return t
        
    def t_VARS(t):
        r"Vars"
        return t
        
    def t_PROCEDURE(t):
        r"Procedure"
        return t
        
    def t_VAR(t):
        r"Var"
        return t
        
    def t_TILDE(t):
        r"~"
        return t
        
    def t_SEMICOLON(t):
        r";"
        return t
        
    def t_COMMA(t):
        r","
        return t
        
    def t_LPAREN(t):
        r"\("
        return t
        
    def t_RPAREN(t):
        r"\)"
        return t
    
    def t_LBRACE(t):
        r"\{"
        return t
        
    def t_RBRACE(t):
        r"\}"
        return t
        
    def t_ASSIGN(t):
        r":="
        return t
        
    def t_IF(t):
        r"If"
        return t
        
    def t_ELSEIF(t):
        r"ElseIf"
        return t
        
    def t_ELSE(t):
        r"Else"
        return t
        
    def t_WHILE(t):
        r"While"
        return t
        
    def t_FOR(t):
        r"For"
        return t
    
    def t_TO(t):
        r"To"
        return t
        
    def t_BY(t):
        r"By"
        return t
        
    def t_RETURN(t):
        r"Return"
        return t
        
    def t_PLUS(t):
        r"\+"
        return t
        
    def t_MINUS(t):
        r"\-"
        return t
        
    def t_STAR(t):
        r"\*"
        return t
        
    def t_SLASH(t):
        r"\/"
        return t
        
    def t_LE(t):
        r"<="
        return t
        
    def t_LT(t):
        r"<"
        return t
        
    def t_GE(t):
        r">="
        return t
        
    def t_GT(t):
        r">"
        return t
        
    def t_EQ(t):
        r"=="
        return t
        
    def t_NE(t):
        r"!="
        return t
        
    def t_AND(t):
        r"&&"
        return t
        
    def t_OR(t):
        r"\|\|"
        return t
        
    def t_NOT(t):
        r"!"
        return t
        
    
    #Variant tokens
    def t_FLOAT(t):
        r"(0|[0-9+])\.[0-9]+"
        t.value = float(t.value)
        return t
        
    def t_INT(t):
        r"0|[0-9]+"
        t.value = int(t.value)
        return t
        
    def t_BOOL(t):
        r"True|False"
        t.value = t.value == "True"
        return t        
        
    def t_QUALIDENT(t):
        r"[a-zA-Z_][a-zA-Z0-9_]*\.[a-zA-Z_][a-zA-Z0-9_]*"
        return t
        
    def t_IDENT(t):
        r"[a-zA-Z_][a-zA-Z0-9_]*"
        return t
        
    return lex.lex()    
lexer = make_lexer()
