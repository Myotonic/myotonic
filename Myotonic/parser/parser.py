from Myotonic.AST import AST
import yacc                     #Taken from ply package

class MyotonicParser(object):
    tabmodule = "parsetab"
    from Myotonic.parser.tokens import tokens
    def __init__(self):
        import Myotonic.parser.lexer
        self.parser = yacc.yacc(module=self, tabmodule=self.tabmodule)
        
    #Common for module and definition parser
    def p_arg_decls_empty(self, p):
        """arg_decls : """
        p[0] = []
        
    def p_arg_decls_single(self, p):
        """arg_decls : arg_decl"""
        p[0] = [p[1]]
        
    def p_arg_decls_multi(self, p):
        """arg_decls : arg_decls COMMA arg_decl"""
        p[0] = p[1] + [p[3]]
        
    def p_arg_decl(self, p):
        """arg_decl : IDENT LBRACE opt_var_arg IDENT RBRACE"""
        p[0] = AST.ArgumentNode(name=p[1], ty=p[4], is_variable=p[3], line=p.lineno(1))
    
    def p_opt_var_arg(self, p):
        """opt_var_arg : VAR
                       | """
        p[0] = len(p) > 1

class ModuleParser(MyotonicParser):
    tabmodule = "parsetab_module"
    start = "module"
    precedence = (
                ("left", "OR"),
                ("left", "AND"),
                ("right", "NOT"),
                ("left", "EQ", "NE", "LT", "LE", "GT", "GE"),
                ("left", "PLUS", "MINUS"),
                ("left", "STAR", "SLASH"),
                ("right", "UNARY"),
                )
                
    #Module
    def p_module(self, p):
        """module : MODULE IDENT imports global_vars procedure_decls TILDE"""
        p[0] = AST.ModuleNode(name=p[2], imports=p[3], global_vars=p[4], procedures=p[5], line=p.lineno(1))
        
    
    #Imports
    def p_imports_empty(self, p):
        """imports : """
        p[0] = []
        
    def p_imports(self, p):
        """imports : IMPORTS import_seq TILDE"""
        p[0] = p[2]
        
    def p_import_seq_empty(self, p):
        """import_seq : """
        p[0] = []
        
    def p_import_seq(self, p):
        """import_seq : IDENT COMMA import_seq"""
        p[0] = [AST.ImportNode(name=p[1], line=p.lineno(1))] + p[3]
        
        
    #Global vars
    def p_global_vars_empty(self, p):
        """global_vars : """
        p[0] = []
    
    def p_global_vars(self, p):
        """global_vars : VARS global_var_seq TILDE"""
        p[0] = p[2]
        
    def p_global_var_seq_empty(self, p):
        """global_var_seq : """
        p[0] = []
        
    def p_global_var_seq(self, p):
        """global_var_seq : IDENT ASSIGN litteral SEMICOLON global_var_seq"""
        p[0] = [AST.GlobalVarNode(name=p[1], value=p[3], line=p.lineno(1))] + p[5]
        
        
    #Procedure declarations
    def p_procedure_decls_empty(self, p):
        """procedure_decls : """
        p[0] = []
        
    def p_procedure_decls(self, p):
        """procedure_decls : procedure procedure_decls"""
        p[0] = [p[1]] + p[2]
        
    def p_procedure(self, p):
        """procedure : PROCEDURE LBRACE IDENT RBRACE IDENT LPAREN arg_decls RPAREN stmts TILDE"""
        p[0] = AST.ProcedureNode(name=p[5], ret_ty=p[3], args=p[7], body=p[9], line=p.lineno(1))
      
      
    #Statements    
    def p_stmts_empty(self, p):
        """stmts : """
        p[0] = []
        
    def p_stmts(self, p):
        """stmts : stmt stmts"""
        p[0] = [p[1]] + p[2]
        
    def p_stmt(self, p):
        """stmt : if_stmt
                | while_stmt
                | for_stmt
                | declare_stmt
                | assign_stmt
                | return_stmt
                | expr SEMICOLON"""
        p[0] = p[1]
        
    def p_if_stmt(self, p):
        """if_stmt : IF LPAREN expr RPAREN stmts elseif_seq opt_else TILDE"""
        p[0] = AST.IfNode(condition=p[3], then_stmts=p[5], elseifs=p[6], else_stmts=p[7], line=p.lineno(1))
        
    def p_elseif_seq_empty(self, p):
        """elseif_seq : """
        p[0] = []
        
    def p_elseif_seq(self, p):
        """elseif_seq : ELSEIF LPAREN expr RPAREN stmts elseif_seq"""
        p[0] = [AST.ElseIfNode(condition=p[3], then_stmts=p[5], line=p.lineno(1))] + p[6]
        
    def p_opt_else_empty(self, p):
        """opt_else : """
        p[0] = []
        
    def p_opt_else(self, p):
        """opt_else : ELSE stmts"""
        p[0] = p[2]
    
    def p_while_stmt(self, p):
        """while_stmt : WHILE LPAREN expr RPAREN stmts TILDE"""
        p[0] = AST.WhileNode(condition=p[3], body=p[5], line=p.lineno(1))
    
    def p_for_stmt(self, p):
        """for_stmt : FOR IDENT ASSIGN expr TO expr opt_by stmts TILDE"""
        p[0] = AST.ForNode(var=p[2], start=p[4], stop=p[6], step=p[7], body=p[8], line=p.lineno(1))
    
    def p_opt_by_empty(self, p):
        """opt_by : """
        p[0] = None
        
    def p_opt_by(self, p):
        """opt_by : BY expr"""
        p[0] = p[2]
    
    def p_declare_stmt(self, p):
        """declare_stmt : IDENT LBRACE IDENT RBRACE SEMICOLON"""
        p[0] = AST.VariableDeclarationNode(name=p[1], ty=p[3], line=p.lineno(1))
    
    def p_assign_stmt(self, p):
        """assign_stmt : IDENT ASSIGN expr SEMICOLON"""
        p[0] = AST.AssignNode(name=p[1], value=p[3], line=p.lineno(1))
    
    def p_return_stmt(self, p):
        """return_stmt : RETURN expr SEMICOLON"""
        p[0] = AST.ReturnNode(value=p[2], line=p.lineno(1))
        
        
    #Expressions
    def p_expr_parens(self, p):
        """expr : LPAREN expr RPAREN"""
        p[0] = p[2]
        
    def p_expr(self, p):
        """expr : litteral
                | binop
                | unop
                | call
                | variable"""
        p[0] = p[1]
    
    def p_litteral(self, p):
        """litteral : INT
                    | FLOAT
                    | BOOL"""
        p[0] = AST.LitteralNode(value=p[1], line=p.lineno(1))
        
    def p_binop(self, p):
        """binop : expr PLUS expr
                 | expr MINUS expr
                 | expr STAR expr
                 | expr SLASH expr
                 | expr EQ expr
                 | expr NE expr
                 | expr LT expr
                 | expr LE expr
                 | expr GT expr
                 | expr GE expr
                 | expr AND expr
                 | expr OR expr"""
        p[0] = AST.BinOpNode(left=p[1], operator=p[2], right=p[3], line=p.lineno(2))
        
    def p_unop(self, p):
        """unop : NOT expr
                | MINUS expr %prec UNARY
                | PLUS expr %prec UNARY"""
        p[0] = AST.UnOpNode(operator=p[1], value=p[2], line=p.lineno(1))
        
    def p_call(self, p):
        """call : IDENT LPAREN call_args RPAREN
                | QUALIDENT LPAREN call_args RPAREN"""
        p[0] = AST.CallNode(name=p[1], args=p[3], line=p.lineno(1))
        
    def p_call_args_empty(self, p):
        """call_args : """
        p[0] = []
        
    def p_call_args_single(self, p):
        """call_args : expr"""
        p[0] = [p[1]]
        
    def p_call_args_multi(self, p):
        """call_args : expr COMMA call_args"""
        p[0] = [p[1]] + p[3]
        
    def p_variable(self, p):
        """variable : IDENT"""
        p[0] = AST.VariableNode(name=p[1], line=p.lineno(1))
        
class DefinitionParser(MyotonicParser):
    start = "definition"
    tabmodule = "parsetab_definition"
    
    def p_definition(self, p):
        """definition : MODULE IDENT proc_decls TILDE"""
        p[0] = p[3]
        
    def p_proc_decls_empty(self, p):
        """proc_decls : """
        p[0] = []
        
    def p_proc_decls(self, p):
        """proc_decls : proc_decl proc_decls"""
        p[0] = [p[1]] + p[2]
        
    def p_proc_decl(self, p):
        """proc_decl : PROCEDURE LBRACE IDENT RBRACE IDENT LPAREN arg_decls RPAREN"""
        p[0] = (p[5], p[3], p[7])