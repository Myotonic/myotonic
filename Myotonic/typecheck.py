from Myotonic.AST import visitor
from Myotonic import log

import sys

class TypeChecker(visitor.ASTVisitor):
    def __init__(self):
        self.functions = {}
        self.variables = {}
        self.global_vars = {}
        
    def get_type_of_litteral(self, litt):
        if isinstance(litt, float):
            return "float"
        elif isinstance(litt, bool):
            return "bool"
        elif isinstance(litt, int):
            return "int"
        
    def do_Module(self, node):
        success = True
        for imp in node.imports:
            success = self(imp) and success
            
        for glob in node.global_vars:
            success = self(glob) and success
        
        for process_body in (False, True):
            for proc in node.procedures:
                success = self(proc, process_body=process_body) and success
            
        if not success:
            sys.exit(1)
            
    def do_Import(self, node):
        return all(map(lambda proc: self(proc, module=node.name), node.procedures))
    
    def do_GlobalVariable(self, node):
        if self.global_vars.get(node.name) != None:
            log.error("Redefinition of global variable: %s." % repr(node.name), node.line)
            return False
        ty = self.get_type_of_litteral(node.value)
        self.global_vars[node.name] = ty
        return True
    
    def do_Procedure(self, node, module="", process_body=True):
        pass